﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SonicBloom.Koreo;
using NaughtyAttributes;

[System.Serializable]
public class Song : Item
{
    public string author;
    [HideInInspector] public float percent;
    public string EventID;
    public Koreography koreography;
}
[System.Serializable]
public class Effect : Item
{

}
[System.Serializable]
public class Theme : Item
{
}

public class ItemsManager : MonoSingleton<ItemsManager>
{
    #region CONST
    const string SONG = "SONG";
    const string ISUNLOCKED = "isUnlocked";
    public const string CURRENTSONG = "currentSong";
    #endregion

    #region SONG
    [Header("Song")]
    public GameObject songPrefab;
    public Transform songListView;
    public List<Song> songList;
    #endregion

    #region EFFECT
    [Header("Effect")]
    public List<Song> effectList;
    #endregion

    #region THEME
    [Header("Theme")]
    public List<Song> themeList;
    #endregion

    public override void Initialize()
    {
        foreach (var song in songList)
        {
            var newSong = Instantiate(songPrefab) as GameObject;
            SongView songView = newSong.GetComponent<SongView>();
            //Unlock 1st song
            if (PlayerPrefs.GetString(CURRENTSONG) == "")
            {
                PlayerPrefs.SetString(CURRENTSONG, "SONG 1");
            }
            if (PlayerPrefs.GetInt(SONG + "S1" + ISUNLOCKED) == 0)
            {
                song.isUnlocked = 1;
            }

            songView.Initialize(
                song.ID,
                song.koreography,
                song.EventID,
                song.title,
                song.author,
                song.price,
                song.percent,
                song.isUnlocked
                );
            newSong.transform.SetParent(songListView);
            songView.toggle.group = songListView.gameObject.GetComponent<ToggleGroup>();
        }
    }

    void UpdateData()
    {
        GameObject[] songs = GameObject.FindGameObjectsWithTag("Song");
        foreach (var song in songs)
        {
            SongView songView = song.GetComponent<SongView>();
        }
    }

    void DeleteAll()
    {
        PlayerPrefs.DeleteAll();
    }

    [Button]
    private void UnlockSong()
    {
        PlayerPrefs.SetInt(SONG + "S2" + ISUNLOCKED, 1);
    }
    [Button]
    private void LockSong()
    {
        PlayerPrefs.SetInt(SONG + "2" + ISUNLOCKED, 0);
    }
}
