﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SonicBloom.Koreo;

public class NoteController : MonoBehaviour, IPooledObject
{
    [SerializeField] private GameObject particle;
    [SerializeField] private float scaleMax;
    private KoreoManager koreoManager;
    private SpawnController spawnController;
    private KoreographyEvent evt;
    private float speed;
    private Vector3 localScale;
    float scaleStart = 0.1f;
    float scaleEnd = 0.5f;

    void Initialized()
    {
        this.spawnController = SpawnController.Instance;
        this.speed = spawnController.noteSpeed;
        this.koreoManager = KoreoManager.Instance;
        this.evt = koreoManager.CurrentEvent;
        this.localScale = transform.localScale;
        randY = (float)Random.Range(-0.1f, 0.1f);
    }
    public void OnObjectSpawn()
    {
        Initialized();
        Move();
        localScale.x = 0f;
        transform.localScale = localScale;
        StartCoroutine(C_LerpScale(0f, scaleMax, 0.3f, 0.75f));
    }
    private void Update()
    {
        Move();
        if (transform.position.x <= koreoManager.DeSpawnPosX)
        {
            Disappear();
        }
    }
    float randY;
    void Move()
    {
        float samplesPerUnit = KoreoManager.Instance.SampleRate / speed;
        Vector3 targetPos = spawnController.spawnTarget.position;
        targetPos.y = randY;
        targetPos.x -= (koreoManager.PlayingKoreo.GetLatestSampleTime() - evt.StartSample) / samplesPerUnit;
        transform.position = targetPos;
    }
    void Disappear()
    {
        gameObject.SetActive(false);
    }
    IEnumerator C_LerpScale(float from, float to, float duration, float delay)
    {
        yield return new WaitForSeconds(delay);
        float t = 0;
        while (t < 1)
        {
            t += Time.deltaTime / duration;
            localScale.x = Mathf.Lerp(from, to, t);
            transform.localScale = localScale;
            yield return null;
        }
    }
    Vector2 pos;
    void SpawnParticle()
    {
        for (int i = 0; i < 3; i++)
        {
            Vector2 randPos = new Vector2(pos.x + Random.insideUnitCircle.x * 0.3f - 0.3f, pos.y + Random.insideUnitCircle.y * 0.3f - 0.4f);
            Instantiate(particle, randPos, Quaternion.identity);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            StartCoroutine(C_LerpScale(scaleMax, 0f, 0.2f, 0f));
            pos = transform.position;
            SpawnParticle();
        }
    }
}
