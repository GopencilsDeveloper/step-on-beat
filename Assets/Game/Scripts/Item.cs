﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Item 
{
	public string ID;
	public int isUnlocked;
	public int price;
	public string title;
}