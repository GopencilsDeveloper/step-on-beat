﻿using UnityEngine;
using SonicBloom.Koreo;
using NaughtyAttributes;

public class Example : MonoBehaviour
{
    public float maxHeight = 0.75f;
    public float maxDistance = 1.25f;
    public GameObject light;
    private Vector3 offset;
    public GameObject effect;
    public Transform note;
    private Vector3 notePos;
    public Transform ground;
    private Vector3 groundPos;
    private Rigidbody2D rgb2D;
    private void Start()
    {
        notePos = note.position;
        groundPos = ground.position;
        rgb2D = GetComponent<Rigidbody2D>();
        Koreographer.Instance.RegisterForEvents("02track", OnFireEvent);

        offset = light.transform.position - gameObject.transform.position;
    }
    [Button]
    private void Jump()
    {
        var g = Physics.gravity.magnitude; // get the gravity value
        var vSpeed = Mathf.Sqrt(2 * g * maxHeight); // calculate the vertical speed
        var totalTime = 2 * vSpeed / g; // calculate the total time
        var hSpeed = maxDistance / totalTime; // calculate the horizontal speed
        rgb2D.velocity = new Vector3(hSpeed, vSpeed, 0); // launch the projectile!

        print(totalTime);
    }
    Collision2D temp;
    private void OnCollisionEnter2D(Collision2D other)
    {
        Jump();
        Invoke("Disable", 0f);
        temp = other;

        Vector2 pos = other.gameObject.transform.position;
        for (int i = 0; i < 5; i++)
        {
            Vector2 randPos = new Vector2(pos.x + Random.insideUnitCircle.x * 0.3f, pos.y + Random.insideUnitCircle.y * 0.3f);
            Instantiate(effect, randPos, Quaternion.identity);
        }
    }
    void Disable()
    {
        temp.gameObject.SetActive(false);
    }
    private void GroundFollowPlayer()
    {
        groundPos.x = gameObject.transform.position.x;
        groundPos.y = Mathf.Sin(Time.time);
        ground.position = groundPos;
    }
    private void Update()
    {
        light.gameObject.transform.position = gameObject.transform.position + offset;
        GroundFollowPlayer();
        if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.D))
        {
            // Smash();
        }
    }

    void OnFireEvent(KoreographyEvent evt)
    {
        Smash();
    }

    private void Smash()
    {
        rgb2D.gravityScale = 0.35f;
        rgb2D.velocity = new Vector3(0f, -10f, 0f);
        // float posY = gameObject.transform.position.y + rgb2D.velocity.y * 0.1f;
        // var obj = Instantiate(note, new Vector3(gameObject.transform.position.x, posY, 0f), Quaternion.identity);
        // obj.gameObject.SetActive(true);
    }
}
