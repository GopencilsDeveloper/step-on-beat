﻿using UnityEngine;

public class StarsController : MonoBehaviour
{
    public Transform player;
    public Transform star1;
    public Transform star2;
    public Transform star3;
	
    private Vector3 playerPos;
    private Vector3 star1Pos;
    private Vector3 star2Pos;
    private Vector3 star3Pos;
    private Vector3 targetPos;

    void Initialize()
    {
        playerPos = player.position;
        star1Pos = star1.position;
        star2Pos = star2.position;
        star3Pos = star3.position;
    }
    void Start()
    {
        Initialize();
    }
    void Update()
    {
        FollowTarget();
    }

    private void FollowTarget()
    {
        targetPos.y = player.position.y + 0.5f;
        star1Pos.y = Mathf.Lerp(star1Pos.y, targetPos.y, 0.6f);
        star1.position = star1Pos;
        star2Pos.y = Mathf.Lerp(star2Pos.y, targetPos.y, 0.2f);
        star2.position = star2Pos;
        star3Pos.y = Mathf.Lerp(star3Pos.y, targetPos.y, 0.1f);
        star3.position = star3Pos;
    }
}
