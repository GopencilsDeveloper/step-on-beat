﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SonicBloom.Koreo;
using SonicBloom.Koreo.Players;

[System.Serializable]
public class Koreo
{
    public string eventID;
    public Koreography koreoGraphy;
}

public class KoreoManager : MonoSingleton<KoreoManager>
{
    #region SerializeField
    [Header("LIST KOREOGRAPHY")]
    [SerializeField] List<Koreo> koreoGraphyList;

    [Header("SERIALIZE FIELDS")]
    [SerializeField] private float hitThresholdInMs;
    [SerializeField] private float perfectThresholdInMS;
    #endregion

    #region Params
    private SimpleMusicPlayer smp;
    private AudioSource audioSource;
    private Koreography playingKoreo;
    private string eventID;
    private KoreographyTrackBase rhythmTrack;
    private List<KoreographyEvent> rawEvents;
    private Queue<KoreographyEvent> trackedNodes = new Queue<KoreographyEvent>();
    int accuracy = 0;
    public int pendingIndex = 0;
    private int sampleRate;
    private SpawnController spawner;
    private float deltaSampleInSecs = 0f;
    private float spawnPosX;
    private float deSpawnPosX;
    private KoreographyEvent currentEvent;
    #endregion

    #region Properties
    public float SampleRate { get { return this.sampleRate; } }
    public AudioSource AudioSource { get { return this.audioSource; } }
    public string EventID { get { return this.eventID; } set { eventID = value; } }
    public float DeltaSampleInSecs { get { return this.deltaSampleInSecs; } }
    public KoreographyEvent CurrentEvent { get { return this.currentEvent; } }
    public Koreography PlayingKoreo { get { return this.playingKoreo; } }
    public float DeSpawnPosX { get { return this.deSpawnPosX; } }
    public float SpawnPosX { get { return this.spawnPosX; } }
    #endregion

    #region Methods
    private void Awake()
    {
        smp = GetComponent<SimpleMusicPlayer>();
        audioSource = GetComponent<AudioSource>();
        smp.Stop();
        audioSource.Stop();
        spawner = SpawnController.Instance;

        float cameraOffsetZ = -Camera.main.transform.position.z;
        spawnPosX = Camera.main.ViewportToWorldPoint(new Vector3(1f, 0f, cameraOffsetZ)).x + 1f;
        deSpawnPosX = Camera.main.ViewportToWorldPoint(new Vector3(0f, 0f, cameraOffsetZ)).x - 1f;
    }
    public void Init(int index)
    {
        this.playingKoreo = koreoGraphyList[index].koreoGraphy;
        this.eventID = koreoGraphyList[index].eventID;
        LoadKoreo();
    }
    public void Init(Koreography koreoGraphy, string eventID)
    {
        this.playingKoreo = koreoGraphy;
        this.eventID = eventID;
        LoadKoreo();
    }
    public void LoadKoreo()
    {
        smp.LoadSong(playingKoreo, 0, false);
        rhythmTrack = playingKoreo.GetTrackByID(eventID);
        rawEvents = rhythmTrack.GetAllEvents();
        sampleRate = playingKoreo.SampleRate;
        Koreographer.Instance.RegisterForEvents(eventID, OnFireEvent);
    }
    public void Play()
    {
        smp.Play();
        audioSource.Play();
    }
    public GameObject portal;
    void Update()
    {
        if (playingKoreo == null)
        {
            return;
        }
        while (trackedNodes.Count > 0 && IsMissed())
        {
            trackedNodes.Dequeue();
        }

        SpawnNext();

        if (Input.GetMouseButtonDown(0))
        {
            CheckNodeHit();
        }
        if (pendingIndex == rawEvents.Count - 1)
        {
            // portal.SetActive(true);
        }
    }

    void CheckNodeHit()
    {
        if (trackedNodes.Count > 0 && IsHit())
        {
            trackedNodes.Dequeue();
        }
    }
    void SpawnNext()
    {
        int currTimeInSample = playingKoreo.GetLatestSampleTime();
        int samplesToTarget = GetSpawnSampleOffset();

        while (pendingIndex < rawEvents.Count &&
        rawEvents[pendingIndex].StartSample < currTimeInSample + samplesToTarget)
        {
            currentEvent = rawEvents[pendingIndex];
            trackedNodes.Enqueue(currentEvent);
            if (pendingIndex + 1 < rawEvents.Count)
            {
                deltaSampleInSecs = (float)(rawEvents[pendingIndex + 1].StartSample - rawEvents[pendingIndex].StartSample) / sampleRate;
            }
            spawner.Spawn();
            pendingIndex++;
        }
    }

    public int GetSpawnSampleOffset()
    {
        float dist = Mathf.Abs(spawnPosX - spawner.spawnTarget.position.x);
        double secsToTarget = (double)dist / (double)spawner.noteSpeed;
        return (int)(secsToTarget * sampleRate);
    }

    private bool IsMissed()
    {
        bool isMissed = false;
        int currTimeInSample = playingKoreo.GetLatestSampleTime();
        int nodeTime = trackedNodes.Peek().StartSample;
        isMissed = Mathf.Abs(currTimeInSample - nodeTime) > (0.001f * playingKoreo.SampleRate * hitThresholdInMs);
        return isMissed;
    }

    private bool IsHit()
    {
        bool isHit = false;
        int currTimeInSample = playingKoreo.GetLatestSampleTime();
        int nodeTime = trackedNodes.Peek().StartSample;
        float offset = Mathf.Abs(currTimeInSample - nodeTime);
        isHit = offset < (0.001f * playingKoreo.SampleRate * hitThresholdInMs);

        float offestInMs = Mathf.Abs(offset / playingKoreo.SampleRate) * 1000f;
        if (offestInMs <= perfectThresholdInMS)
        {
            accuracy = 0;
        }
        else
        if (offestInMs > perfectThresholdInMS && offestInMs <= hitThresholdInMs)
        {
            accuracy = 1;
        }
        return isHit;
    }

    private void OnFireEvent(KoreographyEvent evt)
    {

    }
    #endregion
}
