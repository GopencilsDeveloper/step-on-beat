﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoSingleton<SpawnController>
{
    public Transform spawnTarget;
    public float noteSpeed;

    private ObjectPooler objectPooler;
    public override void Initialize()
    {
        objectPooler = ObjectPooler.Instance;
    }
    public void Spawn()
    {
        var obj = objectPooler.SpawnFromPool("Note", spawnTarget.position, Quaternion.identity);
    }
}
