﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SonicBloom.Koreo;

public class GameManager : MonoSingleton<GameManager>
{
    KoreoManager koreoManager;
    PlayerController playerController;
    public AudioSource audioCom;
    public override void Initialize()
    {
        koreoManager = KoreoManager.Instance;
        playerController = PlayerController.Instance;
    }
    public void StartGame()
    {
        koreoManager.Play();
    }

    public void EndGame()
    {
        // Reset the audio.
        audioCom.Stop();
        audioCom.time = 0f;

        // Flush the queue of delayed event updates.  This effectively resets the Koreography and ensures that
        //  delayed events that haven't been sent yet do not continue to be sent.
        Koreographer.Instance.FlushDelayQueue(koreoManager.PlayingKoreo);

        // Reset the Koreography time.  This is usually handled by loading the Koreography.  As we're simply
        //  restarting, we need to handle this ourselves.
        koreoManager.PlayingKoreo.ResetTimings();
        koreoManager.pendingIndex = 0;
        playerController.RestartPos();

    }

    public void Restart()
    {
        EndGame();
        playerController.RestartPos();
        StartGame();
    }

}
