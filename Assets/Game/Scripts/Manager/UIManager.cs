﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoSingleton<UIManager>
{
    #region Serialize Fields
    
    #endregion
    private string APPSTORE_SHARE_LINK = "https://itunes.apple.com/app/id";
    public GameObject menu;
    public GameObject info;
    public List<GameObject> listPages;
    public Text currentSong;

    public override void Initialize()
    {
        CloseInfo();
        UpdateCurrentSong();
    }


    public void ShowPage(string pageTag)
    {
        foreach (var page in listPages)
        {
            if (page.CompareTag(pageTag))
            {
                StartCoroutine(C_FadeInPage(page));
            }
            else
            {
                StartCoroutine(C_FadeOutPage(page));
            }
        }
    }

    public IEnumerator C_FadeOutPage(GameObject page)
    {
        var animator = page.GetComponent<Animator>();
        animator.SetTrigger("FadeOut");
        yield return new WaitForSeconds(animator.GetCurrentAnimatorStateInfo(0).length);
        page.SetActive(false);
    }

    public IEnumerator C_FadeInPage(GameObject page)
    {
        yield return new WaitForSeconds(0.5f);//Wait for previous Page finishes FadeOut
        page.SetActive(true);
    }

    public void ShowInfo()
    {
        StartCoroutine(C_FadeOutPage(menu));
        StartCoroutine(C_FadeInPage(info));
    }
    public void CloseInfo()
    {
        StartCoroutine(C_FadeOutPage(info));
        StartCoroutine(C_FadeInPage(menu));
    }

    public void PromoGame(string appleID)
    {
        Application.OpenURL(APPSTORE_SHARE_LINK + appleID);
    }

    public void UpdateCurrentSong()
    {
        currentSong.text = PlayerPrefs.GetString("currentSong");
    }
}
