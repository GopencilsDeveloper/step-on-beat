﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Particle : MonoBehaviour
{
    private Vector2 startPos;
    private Transform targetPos;

    public void Start()
    {
        this.startPos = transform.position;
        this.targetPos = GameObject.Find("Circle").transform;
    }
    float t = 0f;
    void LerpToTarget()
    {
        t += Time.deltaTime;
        transform.position = Vector2.Lerp(startPos, targetPos.position, t);
        if (t > 1)
        {
            Destroy(gameObject);
        }
    }
    private void Update()
    {
        LerpToTarget();
    }
}
