﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SonicBloom.Koreo.Demos;
using UnityEngine.EventSystems;
public class PlayerController : MonoSingleton<PlayerController>
{
    private Vector2 position;
    private Rigidbody2D rgb2D;
    private KoreoManager koreoManager;
    public override void Initialize()
    {
        position = transform.position;
        rgb2D = GetComponent<Rigidbody2D>();
        RestartPos();
        this.koreoManager = KoreoManager.Instance;
    }
    public void RestartPos()
    {
        rgb2D.gravityScale = 0;
        rgb2D.velocity = Vector2.zero;
        transform.position = new Vector3(-1f, 1f, -5f);
    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            rgb2D.gravityScale = 0.35f;
            Jump(koreoManager.DeltaSampleInSecs);
        }
        // if (transform.position.x != -1f)
        // {
        //     transform.position = new Vector3(-1f, transform.position.y, transform.position.z);
        // }
    }
    public void Jump(float deltaSec)
    {
        if (deltaSec < 1.3f)
        {
            rgb2D.velocity = new Vector2(0, -7f);
        }
        if (deltaSec >= 1.3f)
        {
            rgb2D.velocity = new Vector2(0, -12f);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "GoodArea")
        {
            // print("Good");
        }
        if (other.tag == "PerfectArea")
        {
            // print("Perfect");
        }
    }
    public GameObject particle;
    private void OnCollisionEnter2D(Collision2D other)
    {
        Vector2 pos = other.gameObject.transform.position;
        for (int i = 0; i < 3; i++)
        {
            Vector2 randPos = new Vector2(pos.x + Random.insideUnitCircle.x * 0.3f, pos.y + Random.insideUnitCircle.y * 0.3f);
            Instantiate(particle, randPos, Quaternion.identity);
        }
    }
}


