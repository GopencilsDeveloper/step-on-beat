﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SonicBloom.Koreo;

public class SongView : MonoBehaviour
{
    private string ID;
    private string eventID;
    private Koreography koreoGraphy;
    public string SongID { get { return this.ID; } }
    public Text title;
    public Text author;
    public Text price;
    public Text percent;
    public Toggle toggle;
    public GameObject buy;

    public void Initialize(string ID, Koreography koreoGraphy, string eventID, string title, string author, int price, float percent, int isUnlocked)
    {
        this.ID = ID;
        this.koreoGraphy = koreoGraphy;
        this.eventID = eventID;
        this.title.text = title.ToUpper();
        this.author.text = author.ToUpper();
        this.price.text = price.ToString();
        this.toggle.interactable = isUnlocked == 1 ? true : false;
        this.percent.gameObject.SetActive(this.toggle.interactable);
        this.percent.text = (percent * 100) + "%";
        this.buy.SetActive(!this.toggle.interactable);
    }

    private void Start()
    {
        if (PlayerPrefs.GetString("currentSong") == this.title.text.ToUpper())
        {
            toggle.isOn = true;
            KoreoManager.Instance.Init(this.koreoGraphy, this.eventID);
        }
        else
        {
            toggle.isOn = false;
        }
        toggle.onValueChanged.AddListener((bool on) => { ToggleValueChanged(on); });
    }

    void ToggleValueChanged(bool on)
    {
        if (on)
        {
            print("toggle is on.");
            PlayerPrefs.SetString("currentSong", this.title.text.ToUpper());
            UIManager.Instance.UpdateCurrentSong();
            KoreoManager.Instance.Init(this.koreoGraphy, this.eventID);
        }
        else
        if (!on)
        {
            print("toggle is off.");
        }
    }
}
